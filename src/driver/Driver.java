package driver;

/*
 * Classe fonctionnel NXT
 * 
 * 
 */

import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;

//import json.JSONObject;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;

@SuppressWarnings("deprecation")
public class Driver {
	final static int BOUTON_A = 0;
	final static int BOUTON_B = 1;
    final static int BOUTON_X = 2;
	final static int BOUTON_Y = 3;
	private static HashMap<String,CharSequence> map = new HashMap<String,CharSequence>();
	
	public static void decode(String msg){
		map.clear();
		
		map.put("B", msg.subSequence(msg.indexOf('['), msg.indexOf('P')-2));
		map.put("P", msg.subSequence(msg.indexOf('P')+3, msg.indexOf('X')-2));
		
		map.put("X", msg.subSequence(msg.indexOf('X')+3, msg.indexOf('Y')-2));
		//LCD.drawString("index: " +msg.indexOf('}'),0,4);
		map.put("Y", msg.subSequence(msg.indexOf('Y')+3, msg.indexOf('}')));
	}
	
	private static void debug(){
		/* Structure du data
		 * {B:[000000000],P:0,X:0.1321432432,Y:-1,0}
		 * 
		 * */
		
		if(!map.isEmpty())
		{
			CharSequence ma_p = map.get("P");
			LCD.drawString("Pad: " + ma_p+"   " , 0, 2);
			CharSequence ma_b = map.get("B");
			LCD.drawString("" + map.get("B").charAt((0*3)+1),0,3);
			LCD.drawString("" + map.get("B").charAt((1*3)+1),1,3);
			LCD.drawString("" + map.get("B").charAt((2*3)+1),2,3);
			LCD.drawString("" + map.get("B").charAt((3*3)+1),3,3);
			CharSequence ma_x = map.get("X"); // Limite l'affichage du tableau a 6 digit (-1,234)
			LCD.drawString("Cond: " + Motor.B.getPosition() , 0, 4);
			//LCD.drawString("AxisX: " + ma_x , 0, 4);
			CharSequence ma_y = map.get("Y");
			LCD.drawString("AxisY: " + ma_y , 0, 5);
			LCD.drawString("AxisX: " + ma_x , 0, 6);
		} 
	}
	private static boolean IsPress(int idx){
		//LCD.drawString(""+map.get("B").charAt(idx), idx, 3);
		return map.get("B").charAt((idx*3)+1) == '1';
	}
	private static boolean FlecheDroite(){
		return Integer.valueOf(map.get("P").toString()) > 45 && Integer.valueOf(map.get("P").toString()) < 135;
	}
	private static boolean FlecheGauche(){
		return Integer.valueOf(map.get("P").toString()) > 225 && Integer.valueOf(map.get("P").toString()) < 315;
	}
	
	private static boolean FlecheBas(){
		return Integer.valueOf(map.get("P").toString()) > 135 && Integer.valueOf(map.get("P").toString()) < 225;
	}
	private static void driveJoyStick()
	{
		float vitesseLeft;
		float vitesseRight;
		vitesseLeft = Float.valueOf(map.get("Y").toString())*500;
		vitesseRight = Float.valueOf(map.get("Y").toString())*500;
		Math.abs(vitesseLeft);
		Math.abs(vitesseRight);
		vitesseLeft += Float.valueOf(map.get("X").toString())*250;
		vitesseRight -= Float.valueOf(map.get("X").toString())*250;
		
		
		
		
		if(vitesseLeft > 100)
		{	
			Motor.A.setSpeed(vitesseLeft);
			Motor.A.backward();
		}
		else if(vitesseLeft < -100)
		{	
			Motor.A.setSpeed(vitesseLeft);
			Motor.A.forward();
		}
		else
			Motor.A.stop();
			
		if(vitesseRight > 100)
		{	
			Motor.C.setSpeed(vitesseRight);
			Motor.C.backward();
		}
		else if(vitesseRight < -100)
		{	
			Motor.C.setSpeed(vitesseRight);
			Motor.C.forward();
		}
		else
			Motor.C.stop();
	}
	public static void main(String[] args) throws IOException {
		
		// TODO Auto-generated method stub
		LCD.drawString("En attente", 0, 0);
		Communication com = new Communication();
		LCD.drawString("Connect�!", 0, 0);
		Motor.B.setSpeed(500);		
		Motor.B.resetTachoCount();
		
		Motor.A.setSpeed(9999);
		Motor.C.setSpeed(9999);
		
		long delta, begin = 0;
		// Ajouter la condition de sortie si la manette envoi le boutton #7
		while(Button.ENTER.isUp()){
			begin = System.currentTimeMillis();
			try{
				decode(com.getData());
				//Debut code du robot
				//IsPress(BOUTON_A) ou FlecheBas()/FlecheDroite()/FlecheGauche()
				if(IsPress(BOUTON_X))//FERMER LES PINCES
				{
						Motor.B.rotateTo(50,true);
				}
				if(IsPress(BOUTON_B))//OUVRIR LES PINCES
				{ 
						Motor.B.rotateTo(-50,true);
				}
				driveJoyStick();
				//Fin code du robot				
				debug(); // Affiche les donnees de la manette				
				//Thread.sleep(30);
				com.sendConfirm();
			}catch(Exception e){
				LCD.drawString("e:" + e.toString(), 0, 6);
				break;
			}	
		}
		Button.waitForAnyPress();
	}

}
