package driver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTCommConnector;
import lejos.nxt.comm.NXTCommDevice;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.USB;
import lejos.nxt.remote.*;

public class Communication {
	NXTConnection connection = null;
	DataInputStream dataIn;
	DataOutputStream dataOut;
	//BufferedReader d;
	public Communication(){    
	      LCD.drawString("waiting for BT", 0,1 );
	      connection = Bluetooth.waitForConnection();
	      dataIn = connection.openDataInputStream();
	      dataOut = connection.openDataOutputStream();
	      //connection = USB.waitForConnection();
	}
	
	public String getData() throws IOException{
		BufferedReader d = new BufferedReader(new InputStreamReader(dataIn));
      return d.readLine();
	}
	
	public void sendConfirm() throws IOException{
		dataOut.writeUTF("SendNext");
		dataOut.flush();
	}
	
	public void disconnect() throws IOException{
		dataOut.close();
	}
	
	public void printReception() throws IOException{
      LCD.drawString(getData(), 0, 0);
	}
}

